import {mapGetters} from 'vuex';
import {convertToObject, lockBody, unlockBody} from '../assets/js/utils/commonUtils';

export const realtyFilter = {
    data() {
        return {
            specs: {},
            facets: {},
            values: {},
            defaultValues: {},
            lots: [],
            count: 0,
            isResetActive: false,

            pageInfo: {
                first: 16,
                after: null,
                hasNext: false,
            },

            view: 'cards',

            flags: {
                isLoading: false,
                isReloading: false,
                isTicking: false,

                // for mobile
                isFilterVisible: false,
            },
        };
    },

    computed: {
        ...mapGetters({
            activeCityId: 'getActiveCityId',
        }),

        vars() {
            const vars = {};

            Object.keys(this.values).forEach(key => {
                if (Array.isArray(this.values[key])) {
                    if (this.values[key].length) {
                        vars[key] = this.values[key];
                    }
                } else if (this.values[key] !== '' && this.values[key] !== null) {
                    vars[key] = this.values[key];
                }
            });

            return vars;
        },

        cleanedLots() {
            return this.lots.length ? this.lots.map(item => item.node) : [];
        },
    },

    mounted() {
        document.addEventListener('scroll', this.handleScroll);
        this.defaultValues = {...this.defaultValues, ...{city: this.activeCityId}};
    },

    beforeDestroy() {
        document.removeEventListener('scroll', this.handleScroll);
    },

    methods: {
        resetPageInfo() {
            this.pageInfo.hasNext = true;
            this.pageInfo.after = '';
        },

        async updateFacets() {
            const {data} = await this.fetchFacets();

            this.facets = convertToObject(data[this.facetField].facets);
            this.count = data[this.facetField].count;
        },

        async updateLots() {
            this.flags.isReloading = true;
            const start = Date.now();
            const {data} = await this.fetchLots();
            const stop = Date.now();

            if (stop - start < 200) {
                setTimeout(() => {
                    this.lots = data[this.lotField].edges;
                    this.pageInfo.after = data[this.lotField].pageInfo.endCursor;
                    this.pageInfo.hasNext = data[this.lotField].pageInfo.hasNextPage;

                    this.$nextTick(() => {
                        this.flags.isReloading = false;
                    });
                }, 200 - (stop - start));
            } else {
                this.lots = data[this.lotField].edges;
                this.pageInfo.after = data[this.lotField].pageInfo.endCursor;
                this.pageInfo.hasNext = data[this.lotField].pageInfo.hasNextPage;

                this.$nextTick(() => {
                    this.flags.isReloading = false;
                });
            }
        },

        async appendLots() {
            this.flags.isLoading = true;

            const {data} = await this.fetchLots();
            this.lots = this.lots.concat(data[this.lotField].edges);
            this.pageInfo.after = data[this.lotField].pageInfo.endCursor;
            this.pageInfo.hasNext = data[this.lotField].pageInfo.hasNextPage;

            this.$nextTick(() => {
                this.flags.isLoading = false;
            });
        },

        updateQuery() {
            this.$router.replace({
                query: this.vars,
            }).catch(err => {
                console.log(err);
            });
        },


        // Event listeners
        onValueChange(val) {
            this.values = {...this.values, ...val};
            this.isResetActive = JSON.stringify(this.values) !== JSON.stringify(this.defaultValues);
            this.resetPageInfo();
            this.updateFacets();
            this.updateLots();
            this.updateQuery();
        },

        onSortChange(val) {
            this.values = {...this.values, ...val};
            this.resetPageInfo();
            this.updateLots();
            this.updateQuery();
        },

        onReset() {
            if (JSON.stringify(this.values) !== JSON.stringify(this.defaultValues)) {
                this.values = {...this.defaultValues};
                this.isResetActive = false;

                this.resetPageInfo();
                this.updateFacets();
                this.updateLots();
                this.updateQuery();
            }
        },

        onViewChange(val) {
            if (this.view === val) {
                return;
            }
            this.flags.isReloading = true;
            this.view = val;
            setTimeout(() => {
                this.$nextTick(() => {
                    this.flags.isReloading = false;
                });
            }, 350);
        },

        onToogleFilter() {
            this.flags.isFilterVisible = !this.flags.isFilterVisible;
            this.flags.isFilterVisible
                ? lockBody()
                : unlockBody();
        },

        handleScroll() {
            if (!this.pageInfo.hasNext || this.flags.isLoading || this.flags.isReloading || this.flags.isTicking) {
                return;
            }

            this.flags.isTicking = true;
            requestAnimationFrame(() => {
                if (window.pageYOffset > this.$refs.page.offsetHeight - (window.innerHeight * 2.5)) {
                    this.appendLots();
                }
                this.flags.isTicking = false;
            });
        },
    },
};
