import {throttle} from '../assets/js/utils/commonUtils';

export const resizeMixin = {
    data() {
        return {
            throttleResize: null,
        };
    },

    mounted() {
        this.throttleResize = throttle(this.handleResize, 1000);
        window.addEventListener('resize', this.throttleResize);
    },

    beforeDestroy() {
        window.removeEventListener('resize', this.throttleResize);
    },
};
