import Vue from 'vue';

import Clickoutside from './clickoutside';
import Intersect from './intersect';

Vue.directive('clickoutside', Clickoutside);
Vue.directive('intersect', Intersect);
