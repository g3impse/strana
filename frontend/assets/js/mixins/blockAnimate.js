export const blockAimate = {
    data() {
        return {
            isVisible: false,
        };
    },

    methods: {
        showBlock(entries, observer, isIntersecting) {
            if (isIntersecting) {
                this.isVisible = true;
            }
        },
    },
};
