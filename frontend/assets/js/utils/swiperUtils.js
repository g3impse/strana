export function formatFraction(number) {
    return number < 10 ? `0${number}` : `${number}`;
}

export function renderFraction(currentClass, totalClass) {
    return `<span class="${currentClass}"></span>&thinsp;/&thinsp;<span class="${totalClass}"></span>`;
}
