let L;
if (process.browser) {
    L = require('leaflet');
}

export const mapToken = 'pk.eyJ1Ijoic3RyYW5hIiwiYSI6ImNraGFsdmx5MjEyMXgycW56cDltdGE0OHIifQ.NjmLmoh71fzKURK9xJIxAw';
// export const mapStyles = `https://api.mapbox.com/styles/v1/alia-new/cjze30p7e01ap1cpb7uvep973/tiles/256/{z}/{x}/{y}@2x?access_token=${mapToken}`;
export const mapStyles = `https://api.mapbox.com/styles/v1/strana/ckhamenz959ah19t99hzr9imu/tiles/256/{z}/{x}/{y}@2x?access_token=${mapToken}`;

export function initMap(container, options = {}) {
    if (!container) {
        console.warn('[mapUtils/initMap] Не укзаан контейнер для карты');
        return;
    }

    const defaultOptions = {
        zoom: 13,
        zoomControl: false,
        scrollWheelZoom: false,
    };

    try {
        return L.map(container, {...defaultOptions, ...options});
    } catch (e) {
        console.error('[mapUtils/initMap] Не удалось инициализировать карту.\n', e);
    }
}

export function getTileLayer() {
    return L.tileLayer(mapStyles, {
        subdomains: ['01', '02', '03', '04'],
        reuseTiles: true,
        updateWhenIdle: false,
    });
}

export function genHtmlProjectMarker(link, item, color) {
    return `<a href="${link}"
                target="_blank"
                class="project-marker _${color}">
                <span class="project-marker__image"
                      style="background-image: url(${item.iconDisplay})">
                </span>
                <div class="project-marker__info">
                    <span class="project-marker__label">
                        Построить маршрут
                    </span>
                    <span class="project-marker__hover-label">
                        Открыть Я.Карты
                    </span>
                    <address class="project-marker__address c-base">
                        ${item.address}
                    </address>
                 </div>
            </a>`;
}

export function genHtmlMarker(coords, html) {
    const defaultHtmlMarker = `
        <div class="circle-marker"></div>
    `;

    const icon = L.divIcon({
        iconSize: null,
        html: html ? html : defaultHtmlMarker,
    });

    return L.marker(coords, {
        icon: icon,
    });
}

export const boundsWithCenter = (b = {}, c = {}) => {
    const bounds = Object.assign({}, b);

    const _lb = bounds._southWest;
    const _rt = bounds._northEast;

    const lat1 = _lb.lat;
    const lat2 = _rt.lat;
    const lat3 = c.lat;

    const latD1 = lat3 - lat1;
    const latD2 = lat2 - lat3;

    if (latD1 < latD2) {
        _lb.lat = lat1 - (latD2 - latD1);
    } else {
        _rt.lat = lat2 + (latD1 - latD2);
    }

    const lng1 = _lb.lng;
    const lng2 = _rt.lng;
    const lng3 = c.lng;

    const lngD1 = lng3 - lng1;
    const lngD2 = lng2 - lng3;

    if (lngD1 < lngD2) {
        _lb.lng = lng1 - (lngD2 - lngD1);
    } else {
        _rt.lng = lng2 + (lngD1 - lngD2);
    }

    return [
        [_lb.lat, _lb.lng],
        [_rt.lat, _rt.lng],
    ];
};
