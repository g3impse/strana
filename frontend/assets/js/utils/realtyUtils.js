export const checkFacets = (facets, values) => Object.entries(facets).reduce((reducer, [key, val]) => (
    {...reducer, ...{[key]: val && val.length < 2 && !values[key] ? val[0] : val.includes(values[key]) ? values[key] : ''}}
), {});

export const cleanValues = values => {
    const vars = {};
    Object.keys(values).forEach(key => {
        if (Array.isArray(values[key])) {
            if (values[key].length) {
                vars[key] = values[key];
            }
        } else if (values[key] !== '' && values[key] !== null) {
            vars[key] = values[key];
        }
    });

    return vars;
};
