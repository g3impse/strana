import allFavoritesIds from '@/queries/favorites/allFavoritesIds.graphql';
import createFavorite from '@/queries/favorites/createFavorite.graphql';
import deleteFavorite from '@/queries/favorites/deleteFavorite.graphql';
import clearFavorite from '@/queries/favorites/clearFavorite.graphql';

export const state = () => ({
    favorites: [],
});

export const actions = {
    async getFavorites({commit}) {
        try {
            const data = await this.$axios.$post('/graphql/', {
                query: allFavoritesIds.loc.source.body,
            }, {
                progress: false,
            });

            let favorites = [];
            const types = ['allGlobalFlats', 'allGlobalParkingSpaces', 'allGlobalCommercialSpaces'];
            types.forEach(type => {
                if (data?.data[type]?.edges?.length) {
                    favorites = [...favorites, ...data.data[type].edges.map(lot => lot.node.id)];
                }
            });

            commit('ADD_FAVORITES', favorites);
        } catch (e) {
            console.log('[Vuex] favorites/readFavoritesFromCookies: Не удалось загрузить', e);
        }
    },

    async changeFavoriteState({state, commit}, flatId) {
        if (!state.favorites.includes(flatId)) {
            try {
                await this.$axios.$post('/graphql/', {
                    query: createFavorite.loc.source.body,
                    variables: {id: flatId},
                }, {
                    progress: false,
                });

                commit('ADD_TO_FAVORITES', flatId);
            } catch (e) {
                console.log('[Vuex] favorites/changeFavoriteState', e);
            }
        } else {
            try {
                await this.$axios.$post('/graphql/', {
                    query: deleteFavorite.loc.source.body,
                    variables: {id: flatId},
                }, {
                    progress: false,
                });

                commit('REMOVE_FROM_FAVORITES', flatId);
            } catch (e) {
                console.log('[Vuex] favorites/changeFavoriteState', e);
            }
        }
    },

    async clearFavorites({commit}) {
        try {
            await this.$axios.$post('/graphql/', {
                query: clearFavorite.loc.source.body,
            });
        } catch (e) {
            console.log('[Vuex] favorites/clearFavorites', e);
        }
        commit('REMOVE_ALL_FAVORITES');
    },
};

export const mutations = {
    ADD_FAVORITES(state, payload) {
        state.favorites = payload;
    },

    ADD_TO_FAVORITES(state, payload) {
        state.favorites.push(payload);
    },

    REMOVE_FROM_FAVORITES(state, payload) {
        state.favorites.splice(state.favorites.indexOf(payload), 1);
    },

    REMOVE_ALL_FAVORITES(state) {
        state.favorites = [];
    },
};
