export const state = () => ({
    is_script_loaded: false,
});

export const actions = {
    setScriptLoaded({commit}) {
        commit('SET_SCRIPT_LOADED', true);
    },
};

export const mutations = {
    SET_SCRIPT_LOADED(state, payload) {
        state.is_script_loaded = payload;
    },
};
