export const state = () => ({
    is_display_small_header: true,
    is_hidden: false,
    is_burger_menu_visible: false,
    header_color: 'milk',
});

export const actions = {
    setSmallHeaderDisplay({commit}, payload) {
        commit('SET_SMALL_HEADER_DISPLAY', payload);
    },

    setHeaderHidden({commit}, payload) {
        commit('SET_HEADER_HIDDEN', payload);
    },

    setHeaderColor({commit}, payload) {
        commit('SET_HEADER_COLOR', payload);
    },

    openBurgerMenu({commit}) {
        commit('SET_BURGER_MENU_VISIBILITY', true);
    },

    closeBurgerMenu({commit}) {
        commit('SET_BURGER_MENU_VISIBILITY', false);
    },
};

export const mutations = {
    SET_SMALL_HEADER_DISPLAY(state, payload) {
        state.is_display_small_header = payload;
    },

    SET_HEADER_HIDDEN(state, payload) {
        state.is_hidden = payload;
    },

    SET_HEADER_COLOR(state, payload) {
        state.header_color = payload;
    },

    SET_BURGER_MENU_VISIBILITY(state, payload) {
        state.is_burger_menu_visible = payload;
    },
};
