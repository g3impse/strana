import allCustomForms from '../queries/allCustomForms.graphql';

export const state = () => ({
    forms: [],
});

export const actions = {
    async getForms({commit}) {
        try {
            const res = await this.$axios.$post('/graphql/', {
                query: allCustomForms.loc.source.body,
            });

            const forms = res?.data?.allCustomForms?.length ? res.data.allCustomForms : [];
            commit('ADD_FORMS', forms);
        } catch (e) {
            console.log('[Vuex] forms/getForms: Не удалось загрузить', e);
        }
    },
};

export const mutations = {
    ADD_FORMS(state, payload) {
        state.forms = payload;
    },
};
