import allCities from '../queries/allCities.graphql';
import allSocials from '../queries/allSocials.graphql';
import sideBlocks from '../queries/sideBlocks.graphql';
import mainMap from '../queries/mainMap.graphql';

export const state = () => ({
    host: null,
    active_city: null,
    cities: [],
    cities_class: {
        '#ed1c24': 'redline',
        '#92278f': 'vice',
        '#c6168d': 'moxxy',
        '#6050bd': 'elysium',
        '#1654c6': 'kain',
    },
    social: [],
    side_blocks: {},
    main_map: {},
});

export const getters = {
    getCityClass: state => color => {
        if (color) {
            return state.cities_class[color] ? state.cities_class[color] : 'vice';
        }

        if (state.active_city?.color) {
            return state.cities_class[state.active_city.color] ? state.cities_class[state.active_city.color] : 'vice';
        }

        return 'vice';
    },
    getActiveCityId: state => state.active_city?.id ? state.active_city.id : '',
    getActiveCityColor: state => state.active_city?.color ? state.active_city.color : '#92278f',
    getActiveCityName: state => state.active_city?.name ? state.active_city.name : '',
    getCityById: state => id => state.cities.find(city => city.id === id),
    getCityProjects: (state, getters) => id => {
        const city = getters.getCityById(id);
        return city ? city.projectSet : [];
    },
    getActiveCities: state => state.cities.filter(city => city.hasActiveProjects),
};

export const actions = {
    async nuxtServerInit({commit, dispatch}, {req}) {
        try {
            const [citiesRes, socialRes, blockRes, mapRes] = await Promise.all([
                this.$axios.$post('/graphql/', {
                    query: allCities.loc.source.body,
                }),

                this.$axios.$post('/graphql/', {
                    query: allSocials.loc.source.body,
                }),

                this.$axios.$post('/graphql/', {
                    query: sideBlocks.loc.source.body,
                }),

                this.$axios.$post('/graphql/', {
                    query: mainMap.loc.source.body,
                }),

                dispatch('favorites/getFavorites'),

                dispatch('forms/getForms'),
            ]);

            const cities = citiesRes.data?.allCities?.edges?.length ? citiesRes.data.allCities.edges.map(item => item.node) : [];
            const social = socialRes.data?.allSocials ? socialRes.data.allSocials.sort((a, b) => a.order - b.order) : [];
            const blocks = blockRes?.data?.mainPage ? blockRes.data.mainPage : {};
            const mainMapData = mapRes?.data?.mainMap ? mapRes.data.mainMap : {};

            commit('SET_CITIES', cities);
            commit('SET_SOCIAL', social);
            commit('SET_SIDE_BLOCKS', blocks);
            commit('SET_MAIN_MAP', mainMapData);
            if (citiesRes.data?.currentCity) {
                commit('SET_ACTIVE_CITY', citiesRes.data.currentCity);
            }

            // await dispatch('favorites/getFavorites');
            // await dispatch('forms/getForms');
        } catch (e) {
            console.log('nuxtServerInit/error', e);
        }

        try {
            process.env.backendUrl
                ? commit('SET_HOST', req.headers.host)
                : commit('SET_HOST', req.headers['x-forwarded-host']);
        } catch (e) {
            console.log('There\'s no request headers host');
        }
    },
};

export const mutations = {
    SET_HOST(state, payload) {
        state.host = payload;
    },

    SET_CITIES(state, payload) {
        state.cities = payload;
    },

    SET_MAIN_MAP(state, payload) {
        state.main_map = payload;
    },

    SET_SOCIAL(state, payload) {
        state.social = [...payload];
    },

    SET_SIDE_BLOCKS(state, payload) {
        state.side_blocks = {...payload};
    },

    SET_ACTIVE_CITY(state, payload) {
        state.active_city = payload;
    },
};
