export const plugins = [
    '~assets/js/directives/index.js',
    '~components/ui/ui.js',
    '~config/plugins/filters',
    '~config/plugins/axios',
    // '~plugins/api',
    '~config/plugins/lazyload',
    '~config/plugins/modal',
    '~config/plugins/vue-qr',
    {
        src: '~config/plugins/vue-mq.js'
    },
];
